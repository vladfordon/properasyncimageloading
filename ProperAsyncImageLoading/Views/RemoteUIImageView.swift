//
//  RemoteUIImageView.swift
//  ProperAsyncImageLoading
//
//  Created by Wladyslaw Jasinski on 09/07/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString,UIImage>()

class RemoteUIImageView: UIImageView {
    var imageUrlString:String?
    
    func loadImageWithUrl(_ url:URL) {
        imageUrlString = url.absoluteString
        image = nil
    
        if let imageFromCache = imageCache.object(forKey: imageUrlString! as NSString) {
            image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error { print(error); return }
            
            DispatchQueue.main.async {
                let imageToCache = UIImage(data:data!)
                if self.imageUrlString == url.absoluteString {
                    self.image = imageToCache
                }
                imageCache.setObject(imageToCache!, forKey: url.absoluteString as NSString)
            }
        }.resume()
    }
}
