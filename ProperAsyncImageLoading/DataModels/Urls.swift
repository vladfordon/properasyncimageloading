//
//  Urls.swift
//  ProperAsyncImageLoading
//
//  Created by Wladyslaw Jasinski on 09/07/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

struct UrlComponents {
    static let host = "https://artbreeder.b-cdn.net/imgs/"
    static let fileSuffix = ".jpeg"
    
    static func imageUrl(_ imageId:String) -> URL {
        return URL(string: UrlComponents.host + imageId + UrlComponents.fileSuffix)!
    }
}
