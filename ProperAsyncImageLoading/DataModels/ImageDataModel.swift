//
//  ImageDataModel.swift
//  ProperAsyncImageLoading
//
//  Created by Wladyslaw Jasinski on 09/07/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

struct ImageDataModel {
    let imageIds =
    [
    "f0a6205ae2d07159c915",
    "9bf702863fa431597461",
    "825f0c8ca4fbcda3e14a",
    "984507242a61b32d1c72",
    "85157dd1be4074c6b144",
    "7ce464f6c4af1dcb8fa2",
    "e983df8dcf2567a98192",
    "876310f919b24cbb6c2e",
    "115d40af8e0016640d7c",
    "22f25c4ff0042e06c70f",
    "f15576c5a5bd4dd1ece8",
    "227befb5cb7043665a9f",
    "254827076349567e9934",
    "0f952575522f3c83d095",
    "b12008e5275477cae744",
    "86bf7ab7277e69db005f",
    "ff87ef513292f06e1338",
    "d81acb9f4056ebf959d9",
    "6e5df10a4f37e067343c",
    "9f4ab410a98bfcd238ad",
    "f59f1126dd787a6ac36c",
    "40d75c6dc3c914b54121",
    "cf247fc392c631aa396d",
    "f633d54041cc849515f0"
    ]
}
