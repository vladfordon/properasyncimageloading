//
//  PhotoCellTableViewCell.swift
//  ProperAsyncImageLoading
//
//  Created by Wladyslaw Jasinski on 09/07/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import UIKit

class PhotoCellTableViewCell: UITableViewCell {
    @IBOutlet weak var bigImage:RemoteUIImageView!
    @IBOutlet weak var idLabel:UILabel!
}
