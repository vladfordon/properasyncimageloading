//
//  ImageViewController.swift
//  ProperAsyncImageLoading
//
//  Created by Wladyslaw Jasinski on 09/07/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation
import UIKit

class ImageViewController:UIViewController, ViewControllerProtocol {
    @IBOutlet weak var imageView:RemoteUIImageView!
    @IBOutlet weak var label:UILabel!
    
    var imageId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        if let imageId = imageId {
            imageView.loadImageWithUrl(UrlComponents.imageUrl(imageId))
            label.text = imageId
        }
    }
    
}
