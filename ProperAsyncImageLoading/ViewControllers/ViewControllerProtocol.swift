//
//  ViewControllerProtocol.swift
//  ProperAsyncImageLoading
//
//  Created by Wladyslaw Jasinski on 09/07/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import Foundation

protocol ViewControllerProtocol {
    func setupView()
}
