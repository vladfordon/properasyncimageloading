//
//  PhotoListController.swift
//  ProperAsyncImageLoading
//
//  Created by Wladyslaw Jasinski on 09/07/2020.
//  Copyright © 2020 Apptsar. All rights reserved.
//

import UIKit

class PhotoListController: UITableViewController, ViewControllerProtocol {
    let imageDataModel = ImageDataModel()
    let cellId = "FaceImageId"
    let detailsViewControllerId = "ImageViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        navigationController?.title = "Faces"
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageDataModel.imageIds.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! PhotoCellTableViewCell
    
        let imageId = imageDataModel.imageIds[indexPath.row]
        cell.bigImage.loadImageWithUrl(UrlComponents.imageUrl(imageId))
        cell.idLabel.text = imageId
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: detailsViewControllerId) as! ImageViewController
        vc.imageId = imageDataModel.imageIds[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}
